# supergfxctl ([-gex]: Gnome extension)

Extension for visualizing [supergfxctl](https://gitlab.com/asus-linux/supergfxctl)(`asusd`) settings and status.

---

## Requirements

* gnome >= 45
* [supergfxctl](https://gitlab.com/asus-linux/supergfxctl) >= 5.0.1

---

## Build Instructions

### Dependencies

* nodejs >= 20.10.0
* npm >= 10.2.0

### Building (production)

In a terminal enter the following commands as a user (**do NOT run as root or sudo**):

```bash
git clone https://gitlab.com/asus-linux/supergfxctl-gex.git /tmp/supergfxctl-gex && cd /tmp/supergfxctl-gex
npm install
npm run build && npm run install-user
```

_HINT: You will need to reload the GNOME Shell afterwards. (`Alt + F2` -> `r` on X11, `logout` on Wayland)_

### Building (development)

Instead of the
`npm run build && npm run install-user`
above, use this line instead:
`npm run build && npm run install-dev`

This will remove any production versions and installs the development version instead.

_HINT: You will need to reload the GNOME Shell afterwards. (`Alt + F2` -> `r` on X11, `logout` on Wayland)_ and probably manually enable the extension again.

### Source debugging

`cd` into the directory where you've downloaded the `supergfxctl-gex` source code and enter the following commands:

```bash
npm install
npm run debug
```

---

## License & Trademarks

**License:** Mozilla Public License Version 2.0 (MPL-2)
