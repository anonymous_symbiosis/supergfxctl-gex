// @ts-ignore: new import-scheme (GS >=45)
import GLib from 'gi://GLib';
// @ts-ignore: new import-scheme (GS >=45)
import Gio from 'gi://Gio';
// @ts-ignore: new import-scheme (GS >=45)
import { Extension } from 'resource:///org/gnome/shell/extensions/extension.js';
// @ts-ignore: new import-scheme (GS >=45)
import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import * as Log from './modules/log';
import * as Panel from './modules/panel';
import { IEnableableModule } from './interfaces/iEnableableModule';

declare var SGFXGEX_PATH: string;

export default class SuperGfxExtension
  extends Extension
  implements IEnableableModule
{
  private gpuModeIndicator?: any = null;
  private quickToggles?: any = null;
  private systemMenu?: any = null;
  private glSourceId?: number | null = null;
  private resources?: any = null;

  /**
   * GJS enable() implementation - enables the extension (called by GS)
   * @returns {boolean} enable state
   */
  public enable(): boolean {
    // @ts-ignore
    globalThis.SGFXGEX_PATH = this.path;

    // new async loading since GS 45
    if (Main.panel.statusArea.quickSettings._system && this.glSourceId !== null)
      return this.enableSystemMenu();
    return this.enableSystemMenuQueued();
  }

  /**
   * enables the systemMenue (lazy)
   */
  private enableSystemMenuQueued(): boolean {
    var success = false;
    this.glSourceId = GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
      if (!Main.panel.statusArea.quickSettings._system)
        return GLib.SOURCE_CONTINUE;

      success = this.enableSystemMenu();
      this.glSourceId = null;
      return GLib.SOURCE_REMOVE;
    });
    return success;
  }

  /**
   * enables the system (indicator) menu
   */
  private enableSystemMenu(): boolean {
    this.systemMenu = Main.panel.statusArea.quickSettings;
    if (this.systemMenu === undefined || this.systemMenu === null) {
      Log.raw('init: system menu is not defined');
      return false;
    }

    // register Gio resources
    this.resources = Gio.Resource.load(
      // @ts-ignore: this.path (was super.path)
      `${this.path}/resources/org.gnome.shell.extensions.supergfxctl-gex.gresource`
    );
    Gio.resources_register(this.resources);

    this.gpuModeIndicator = new Panel.GpuModeIndicator();
    this.quickToggles = new Panel.GpuModeToggle(this.gpuModeIndicator, this);

    // must be populated in any case!
    this.gpuModeIndicator.quickSettingsItems.push(this.quickToggles);
    Main.panel.statusArea.quickSettings.addExternalIndicator(
      this.gpuModeIndicator
    );

    // insert above Background-Processes
    if (this.systemMenu?._backgroundApps?.quickSettingsItems?.at(-1))
      for (const item of this.gpuModeIndicator.quickSettingsItems) {
        this.systemMenu.menu._grid.set_child_below_sibling(
          item,
          this.systemMenu._backgroundApps.quickSettingsItems[0]
        );
      }

    return true;
  }

  /**
   * disables/unloads the extension
   */
  public disable(): void {
    this.quickToggles?.disable();
    this.gpuModeIndicator?.destroy();
    this.quickToggles = null;
    this.gpuModeIndicator = null;

    // removes GLib source if available
    if (this.glSourceId) {
      GLib.Source.remove(this.glSourceId);
      this.glSourceId = null;
    }

    // unregister resources
    Gio.resources_unregister(this.resources);
  }
}
