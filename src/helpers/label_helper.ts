// @ts-ignore: new import-scheme (GS >=45)
import Gio from 'gi://Gio';

import * as Log from '../modules/log';
import { VersionHelper } from './version_helper';
import { VersionNumber } from '../types/version_number';
import { LabelType } from '../types/label_type';
import { ActionType } from '../types/action_type';
import { IconType } from '../types/icon_type';
import { Icon } from './resource_helpers';

export class LabelHelper {
  private _gfxLabels = [
    'hybrid',
    'integrated',
    'vfio',
    'egpu',
    'asusmuxdiscreet',
    'none',
  ];
  private _gfxLabelsMenu = [
    'Hybrid',
    'Integrated',
    'VFIO',
    'eGPU',
    'MUX / dGPU',
    'None',
  ];
  private _powerLabel = [
    'active',
    'suspended',
    'off',
    'disabled',
    'active',
    'unknown',
  ];
  private _powerLabelFilename = [
    'active',
    'suspended',
    'off',
    'off',
    'active',
    'gpu-integrated-active',
  ];
  private _userActions = [
    ActionType.Logout,
    ActionType.Reboot,
    ActionType.Integrated,
    ActionType.AsusMuxDisable,
    ActionType.None,
  ];
  private vHelper!: VersionHelper;

  /**
   * constructor
   * @param {VersionHelper | VersionNumber} vInstatce  VersionHelper OR VersionNumber instance
   */
  constructor(vInstatce: VersionHelper | VersionNumber) {
    Log.raw('initializing...');
    if (vInstatce instanceof VersionHelper) this.vHelper = vInstatce;
    else this.vHelper = new VersionHelper(vInstatce);
  }

  /**
   * @returns {number} gnome-shell version (encapsulated)
   */
  public get gsVersion(): number {
    return this.vHelper.gsVersion();
  }

  /**
   * returns gfx labels for specific type depending on the actual supergfxctl version
   * @param {LabelType} type labeltype to request
   * @returns {string[]} array of corresponding labels (empty on error)
   */
  public gfxLabels(type: LabelType): string[] {
    if (
      !(type === LabelType.Gfx || type === LabelType.GfxMenu) ||
      !this.vHelper.isBetween(
        this.vHelper.currentVersion,
        this.vHelper.allowedSgfxVersions
      )
    ) {
      return []; // unsupported
    }

    switch (type) {
      case LabelType.Gfx:
        return this._gfxLabels
          .slice(0, 2) // first 2 items
          .concat(
            this.v50 ? [] : this.v51 ? ['nvidianomodeset'] : [], // addition (at pos: 3)
            this._gfxLabels.slice(-4) // concatiate the rest
          );
      case LabelType.GfxMenu:
        return this._gfxLabelsMenu
          .slice(0, 2)
          .concat(
            this.v50 ? [] : this.v51 ? ['Nvidia (no modeset)'] : [],
            this._gfxLabelsMenu.slice(-4)
          );
      default:
        return []; // empty
    }
  }

  /**
   * @returns {ActionType[]} list of user actions depending on the actual supergfxctl version
   */
  public get userActions(): ActionType[] {
    return this._userActions
      .slice(0, 1) // first item
      .concat(
        this.v50 ? [] : this.v51 ? [ActionType.Reboot] : [], // addition (at pos: 2)
        this._userActions.slice(-3) // concatiate the rest
      );
  }

  /**
   * Get defined label
   * @param {LabelType} type to aquire from
   * @param {number} idx index (number)
   * @returns {string} empty string if not found
   */
  public get(type: LabelType, idx: number): string {
    switch (type) {
      case LabelType.Gfx:
      case LabelType.GfxMenu:
        return this.gfxLabels(type)[idx];
      case LabelType.Power:
        return this._powerLabel[idx];
      case LabelType.PowerFileName:
        return this._powerLabelFilename[idx];
      default:
        return '';
    }
  }

  /**
   * Get defined Action
   * @param {number} idx index (number)
   * @returns {ActionType | undefined} undefined string if not found or invalid type
   */
  public getAction(idx: number): ActionType | undefined {
    return this.userActions[idx] ?? undefined;
  }

  /**
   * @param {IconType} iconType defines the prefix
   * @param {LabelType} type to aquire from
   * @param {number} lastState LastState index (number)
   * @param {LabelType | undefined} powerType PowerType if any
   * @param {number | undefined} lastPowerState LastState index (number)
   * @returns {Gio.Icon | undefined} undefined string if not found or invalid type
   */
  public getIcon(
    iconType: IconType,
    type: LabelType,
    lastState: number,
    powerType: LabelType | undefined = undefined,
    lastPowerState: number | undefined = undefined
  ): Gio.Icon {
    let label = this.get(type, lastState);
    switch (iconType) {
      case IconType.GPU:
        label = 'gpu-' + label;
        break;
      case IconType.dGPU:
        label = 'dgpu-' + label;
        break;
    }
    if (powerType !== undefined && lastPowerState !== undefined) {
      let powerLabel = this.get(powerType, lastPowerState);
      if (powerLabel === 'active') label += '-active';
    }
    return Icon.getByName(label + '-symbolic'); // we're just using symbolic-icons
  }

  /**
   * @param {LabelType} type to aquire from
   * @param {number} lastState LastState index (number)
   * @param {LabelType | undefined} powerType PowerType if any
   * @param {number | undefined} lastPowerState LastState index (number)
   * @returns {Gio.Icon | undefined} undefined string if not found or invalid type
   */
  public getGpuIcon(
    type: LabelType,
    lastState: number,
    powerType: LabelType | undefined = undefined,
    lastPowerState: number | undefined = undefined
  ): Gio.Icon {
    return this.getIcon(
      IconType.GPU,
      type,
      lastState,
      powerType,
      lastPowerState
    );
  }

  /**
   * @param {LabelType} type to aquire from
   * @param {number} lastState LastState index (number)
   * @param {LabelType | undefined} powerType PowerType if any
   * @param {number | undefined} lastPowerState LastState index (number)
   * @returns {Gio.Icon | undefined} undefined string if not found or invalid type
   */
  public getDGpuIcon(
    type: LabelType,
    lastState: number,
    powerType: LabelType | undefined = undefined,
    lastPowerState: number | undefined = undefined
  ): Gio.Icon {
    return this.getIcon(
      IconType.dGPU,
      type,
      lastState,
      powerType,
      lastPowerState
    );
  }

  /**
   * checks if a requested label equals a provided comparsion string
   * @param {Labeltype} type to aquire from
   * @param {number} idx index
   * @param {string} comp string to compare with
   * @returns {boolean} true if match
   */
  public is(type: LabelType, idx: number, comp: string | ActionType): boolean {
    if (LabelType.UserAction) this.getAction(idx) === <ActionType>comp;
    return this.get(type, idx) === comp;
  }

  private isVersion(major: number = 5, minor: number = 0): boolean {
    return this.vHelper.isBetween(this.vHelper.currentVersion, [
      [major, minor, 0],
      [major, minor, 9999],
    ]);
  }

  // supported versions
  public get v50() {
    return this.isVersion(5, 0);
  }
  public get v51(): boolean {
    return this.isVersion(5, 1);
  }
}
