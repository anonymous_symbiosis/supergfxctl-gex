declare const imports: any;

// @ts-ignore
import * as Util from 'resource:///org/gnome/shell/misc/util.js';
// @ts-ignore
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
// @ts-ignore
import * as MessageTray from 'resource:///org/gnome/shell/ui/messageTray.js';

import * as Resources from './resource_helpers';
import { ActionType } from '../types/action_type';
import { UrgencyLevel } from '../types/urgency_level';

export class PanelHelper {
  /**
   * shows a notification
   * @param {string} details notification message
   * @param {string} icon icon (as string)
   * @param {ActionType} action should be empty by default
   * @param {UrgencyLevel} urgency the urgency-level of the notification
   */
  public static notify(
    details: string,
    icon: string,
    action: ActionType = ActionType.None,
    urgency: UrgencyLevel = UrgencyLevel.Low
  ): void {
    const params = { gicon: Resources.Icon.getByName(icon) };
    const source = new MessageTray.Source('Graphics', icon, params);
    const notification = new MessageTray.Notification(
      source,
      'Graphics',
      details,
      params
    );

    Main.messageTray.add(source);
    notification.setTransient(true);

    switch (action) {
      case ActionType.Logout:
        notification.setUrgency(UrgencyLevel.Critical);
        notification.addAction('Log Out Now!', () => {
          Util.spawnCommandLine('gnome-session-quit');
        });
        break;
      case ActionType.Reboot:
        notification.setUrgency(UrgencyLevel.Critical);
        notification.addAction('Reboot Now!', () => {
          Util.spawnCommandLine('shutdown -ar now');
        });
        break;
      default:
        notification.setUrgency(urgency);
    }

    source.showNotification(notification);
  }
}
