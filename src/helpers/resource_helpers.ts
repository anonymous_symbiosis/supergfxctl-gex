// @ts-ignore: new import-scheme (GS >=45)
import Gio from 'gi://Gio';
// @ts-ignore: new import-scheme (GS >=45)
import St from 'gi://St';
// @ts-ignore: new import-scheme (GS >=45)
import GLib from 'gi://GLib';

import * as Log from '../modules/log';

/**
 * File abstraction to load files for desired purposes
 */
export class File {
  /**
   * loads DBus XML file and return its content as string (byte[] -> string)
   * @param {string} name filename without extension or path
   * @returns {string|undefined} file content
   */
  public static DBus(name: string, path: string = ''): string | undefined {
    // @ts-ignore: https://developer.mozilla.org/en-US/docs/Web/API/TextDecoder
    const decoder = new TextDecoder();
    const file = `${path}/resources/dbus/${name}.xml`;
    try {
      const [ok, bytes] = GLib.file_get_contents(file);
      if (!ok) Log.warn(`Couldn't read contents of "${file}"`);
      return ok ? decoder.decode(bytes) : undefined;
    } catch (e) {
      Log.error(`Failed to load "${file}"`, e);
    }
  }
}

/**
 * Icon helpers
 */
export class Icon {
  /**
   * Returns a themed icon, using fallbacks from GSConnect's GResource when necessary.
   * @param {string} name - A (themed) icon name or path
   * @return {Gio.Icon} A (themed) icon
   */
  public static getByName(name: string): Gio.Icon {
    const iconTheme = new St.IconTheme();
    // @ts-ignore
    iconTheme.prepend_search_path(SGFXGEX_PATH + '/resources/icons');
    /* TODO: not working as expected..
    iconTheme.add_resource_path(
      'resource:///org/gnome/shell/extensions/supergfxctl-gex/icons'
    );
    */

    try {
      if (iconTheme.has_icon(name) && name !== 'gpu-hybrid-active') {
        Log.raw('found icon', name);
        const filename = iconTheme.choose_icon([name], -1, 2).get_filename();
        Log.raw('found icon - filename', filename);
        return new Gio.FileIcon({ file: Gio.File.new_for_path(filename) });
      }
    } catch (ex) {
      Log.error('icon loading error', ex);
    }

    Log.raw('icon not found - falling back to themed ones ');
    return new Gio.ThemedIcon({ name: name });
  }
}
