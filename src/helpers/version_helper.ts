//@ts-ignore
import * as Config from 'resource:///org/gnome/shell/misc/config.js';
const [major] = Config.PACKAGE_VERSION.split('.').map((s: string) => Number(s));

import { VersionNumber } from '../types/version_number';

export class VersionHelper {
  private curVersion!: VersionNumber;
  public allowedSgfxVersions: VersionNumber[] = [
    [5, 0, 0], // min version: 5.0.0
    [5, 2, 9999], // max version <5.2.0
  ];

  /**
   * constructor
   * @param {VersionNumber} version used as current version
   */
  constructor(version: VersionNumber) {
    // correct if the version is mapped as "string"
    this.curVersion = version.map((i) => Number(i)) as VersionNumber;
  }

  /**
   * @returns current version as VersionNumber
   */
  public get currentVersion(): VersionNumber {
    return this.curVersion;
  }

  /**
   * @returns {boolean} if there is an update available (unimplemented for now)
   */
  public get currentVersionHasUpdate(): boolean {
    // TODO: imeplemnt me
    return false;
  }

  /**
   * @returns {number} GNOME Shell version (as number or undefined if lower than 40)
   */
  public gsVersion(): number {
    return major >= 40 ? major : -1;
  }

  /**
   * compares two version numbers
   * @param {VersionNumber} verA Version to compare
   * @param {VersionNumber} verB Version to compare against
   * @returns {number} 0 -> equals | -1 -> lower | 1 -> bigger
   */
  public compare(verA: VersionNumber, verB: VersionNumber): number {
    // map versions
    verA = verA.map((i) => Number(i)) as VersionNumber;
    verB = verB.map((i) => Number(i)) as VersionNumber;

    if (JSON.stringify(verA) === JSON.stringify(verB)) {
      return 0; // equals
    } else if (verA[0] !== verB[0]) {
      return verA[0] < verB[0] ? -1 : 1;
    } else if (verA[1] !== verB[1]) {
      return verA[1] < verB[1] ? -1 : 1;
    } else if (verA[2] !== verB[2]) {
      return verA[2] < verB[2] ? -1 : 1;
    }

    return 0; // as all above aren't matching it must be equal (this point should be impossible to reach)
  }

  /**
   * determines if a version number is between a given range
   * @param {VersionNumber} version to check
   * @param {VersionNumber[]} range contrains version from and version to (equal is be covered)
   * @returns {boolean} is or is not in range
   */
  public isBetween(version: VersionNumber, range: VersionNumber[]): boolean {
    if (range.length < 2) return false;
    return (
      this.compare(version, range[0]) >= 0 &&
      this.compare(version, range[1]) <= 0
    );
  }
}
