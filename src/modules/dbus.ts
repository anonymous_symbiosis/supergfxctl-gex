// @ts-ignore: new import-scheme (GS >=45)
import Gio from 'gi://Gio';

import * as Resources from '../helpers/resource_helpers';
import * as Log from './log';

import { PanelHelper } from '../helpers/panel_helper';
import { LabelHelper } from '../helpers/label_helper';
import { LabelType } from '../types/label_type';
import { VersionNumber } from '../types/version_number';
import { GpuModeToggle } from './panel';
import { UrgencyLevel } from '../types/urgency_level';
import { ActionType } from '../types/action_type';

/**
 * DBus implementation for supergfxd (supergfxctl)
 */
export class DBus {
  public labels!: LabelHelper;
  private gpuModeToggle!: any; // caller DI instance(parent)
  private proxy!: any;
  private sgfxVendor = '';
  private sgfxLastState = 6; // 6 is unknonwn (+1 if >= v5.1.1)
  private sgfxPowerState = 5; // 3 is unknonwn
  private sgfxSupportedGPUs: number[] = [];

  public connected = false;

  constructor(gpuModeToggle: typeof GpuModeToggle) {
    this.gpuModeToggle = gpuModeToggle;
    this.proxy = new Gio.DBusProxy.makeProxyWrapper(
      Resources.File.DBus('org-supergfxctl-gfx-5', this.gpuModeToggle._exPath)
    )(Gio.DBus.system, 'org.supergfxctl.Daemon', '/org/supergfxctl/Gfx');
  }

  /**
   * Default initializer
   */
  public async initialize(): Promise<void> {
    // Log.info('DBus: initializing interface...');

    try {
      await this.initLabels();

      // adjustments if v5.1.1 or above
      if (this.labels.v51) this.sgfxLastState = 7;

      const sup = await this.proxy.SupportedAsync();
      if (Array.isArray(sup) && Array.isArray(sup[0])) {
        this.sgfxSupportedGPUs = sup[0].map((n) => parseInt(n) || 0);
        this.connected = sup[0].length === this.sgfxSupportedGPUs.length;
      }

      if (this.connected) {
        // calling getters to get latest values
        await this.vendor();
        await this.gfxMode();
        await this.gpuPower();

        // connect signals (DBus)
        this.connectNotifySignal();
        this.connectPowerSignal();

        try {
          const lastState = this.lastState;
          const lastPState = this.lastPowerState;
          const lVar = `${this.labels.get(
            LabelType.GfxMenu,
            lastState
          )} (${this.labels.get(LabelType.Power, lastPState)}})`;

          const gpuIcon = this.labels.getGpuIcon(
            LabelType.Gfx,
            lastState,
            LabelType.PowerFileName,
            lastPState
          );

          if (this.labels.gsVersion >= 44) this.gpuModeToggle.title = lVar;
          else this.gpuModeToggle.label = lVar;

          this.gpuModeToggle.gicon = gpuIcon;
          this.gpuModeToggle.menu.setHeader(
            gpuIcon,
            `${this.labels.get(
              LabelType.GfxMenu,
              lastState
            )} (${this.labels.get(LabelType.Power, lastPState)})`
          );
        } catch {
          PanelHelper.notify(
            'DBus: Could not establish connection to supergfxctl!',
            'gpu-integrated-active',
            ActionType.None,
            UrgencyLevel.Critical
          );

          Log.error(
            'DBus: initializing interface: failed after making the connection!'
          );
        }
        // Log.info('DBus: initializing interface: completed.');
      }
    } catch {
      Log.error('DBus: initializing interface: aborted!');
    }
  }

  /**
   * initializes the labels (needed for version compatibility)
   */
  public async initLabels(): Promise<void> {
    // Log.info('DBus: initializing labels...');
    try {
      // instanciate with a version, instead of a VersionHelper instance
      this.labels = new LabelHelper(
        (await this.proxy.VersionAsync()).toString().split('.') as VersionNumber
      );
    } catch {
      Log.error('DBus: initializing labels: get current version failed!');
    }
    // Log.info('DBus: initializing labels: done.');
  }

  /**
   * connects to the gfx-change notify signal-bus (DBus)
   */
  public connectNotifySignal(): void {
    try {
      this.proxy.connectSignal(
        'NotifyAction',
        (_proxy: any = null, _name: string, value: number) => {
          let newMode = parseInt(this.proxy.ModeSync());
          let details = `has changed.`;
          let iconName = 'supergfxctl-gex';
          let switchable = true;

          switch (this.labels.getAction(value)) {
            case ActionType.Integrated:
              details = `You must switch to Integrated mode before switching to VFIO.`;
              switchable = false;
              break;
            case ActionType.None:
              details = `changed to ${this.labels.get(LabelType.Gfx, newMode)}`;
              break;
            default:
              const action = this.labels.getAction(value);
              details = `changed. Please save your work and ${
                action === ActionType.Reboot ? 'reboot' : 'logout'
              } to apply the changes.`;
              iconName =
                action === ActionType.Reboot
                  ? 'system-reboot-symbolic'
                  : 'system-log-out-symbolic';
              break;
          }
          if (switchable && newMode !== this.sgfxLastState) {
            this.sgfxLastState = newMode;
            this.gpuModeToggle.refresh();
          }

          PanelHelper.notify(
            details,
            iconName,
            <ActionType>this.labels.getAction(value)
          );
        }
      );
    } catch {
      Log.error(
        `DBus: connecting signal: Error, no live updates of GPU modes!`
      );
    }
  }

  /**
   * connecting DBus to the power-signal provider/generator
   */
  public connectPowerSignal(): void {
    try {
      this.proxy.connectSignal(
        'NotifyGfxStatus',
        (_proxy: any = null, _name: string, value: number[]) => {
          if (this.sgfxPowerState !== value[0]) {
            this.sgfxPowerState = value[0];

            // if integrated and active show notification
            if (this.sgfxPowerState === 0 && this.sgfxLastState === 1) {
              // let's check the mode
              try {
                let mode = parseInt(this.proxy.ModeSync());
                if (this.labels.is(LabelType.Gfx, mode, ActionType.Integrated))
                  PanelHelper.notify(
                    `Your dedicated GPU turned on while you are on the integrated mode. This should not happen. It could be that another application rescanned your PCI bus. Rebooting is advised.`,
                    'dialog-error',
                    ActionType.Reboot
                  );
                else if (this.sgfxLastState !== mode) this.sgfxLastState = mode;
              } catch {
                Log.error('DBus: getting mode: failed!');
              }
            }
            this.gpuModeToggle.refresh();
          }
        }
      );
    } catch (error) {
      Log.error(
        `DBus: connecting signal: Error, no live updates of power modes!`,
        error
      );
    }
  }

  /**
   * get Vendor from DBus
   * @returns {string} the current gfx-vendor using DBus
   */
  public async vendor(): Promise<string> {
    try {
      this.sgfxVendor = await this.proxy.VendorAsync();
    } catch {
      Log.error('DBus: get current vendor: failed!');
    }
    return this.sgfxVendor;
  }

  /**
   * get GfxMode from DBus
   * @returns {number} the gfx-mode using DBus
   */
  public async gfxMode(mode: number = -1): Promise<number> {
    try {
      if (mode >= 0) {
        try {
          await this.proxy.SetModeAsync(mode);
        } catch (e: any) {
          Log.error('DBus: switching mode: failed!');
          PanelHelper.notify(e.toString(), 'dialog-error');
        }
      }
      this.sgfxLastState = parseInt(await this.proxy.ModeAsync());
    } catch {
      Log.error('DBus: get current mode: failed!');
    }
    return this.sgfxLastState;
  }

  /**
   * get current PowerMode from DBus
   * @returns {number} last gpu-power-state using DBus
   */
  public async gpuPower(): Promise<number> {
    try {
      this.sgfxPowerState = parseInt(
        (await this.proxy.PowerAsync()).toString().trim()
      );
    } catch {
      Log.error('DBus: getting power mode: failed!');
      this.sgfxPowerState = 5;
    }
    return this.sgfxPowerState;
  }

  /**
   * get supported GPUs
   * @returns {number[]} supported gpu-id list, using DBus
   */
  public get supported(): number[] {
    if (this.connected) {
      // check if we're on MUX with no other option
      if (
        this.sgfxSupportedGPUs.length === 1 &&
        ((this.labelHelper.v50 && this.sgfxSupportedGPUs[0] === 4) ||
          (this.labelHelper.v51 && this.sgfxSupportedGPUs[0] === 5))
      ) {
        // (in that case we need to append Hybrid(0))
        return [0].concat(this.sgfxSupportedGPUs);
      }
      return this.sgfxSupportedGPUs;
    }
    return [];
  }

  /**
   * get the last GfxState
   * @returns {number} the last gfx-state
   */
  public get lastState(): number {
    return this.connected ? this.sgfxLastState : 6 + (this.labels.v51 ? 1 : 0);
  }

  /**
   * get the last GfxPowerState
   * @returns {number} the last power-state
   */
  public get lastPowerState(): number {
    return this.connected ? this.sgfxPowerState : 5;
  }

  /**
   * get the labelHelper instance
   * @returns {LabelHelper} instance used by DBus (shared)
   */
  public get labelHelper(): LabelHelper {
    return this.labels;
  }
}
