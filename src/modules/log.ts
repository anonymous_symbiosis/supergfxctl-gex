/**
 * RAW Logging abstraction
 * @param {string} text log message
 * @param {string} prefix a variable prefix like '[TEST]' (optional)
 * @param {any} obj an object to dump (optional)
 */
export function parseLog(
  text: string,
  prefix: string = '',
  obj: any = null
): string {
  if (obj !== null)
    return `supergfxctl-gex: ${prefix}${text}, obj: ${JSON.stringify(obj)}`;
  return `supergfxctl-gex: ${prefix}${text}`;
}

/**
 * logs a message without loglevel
 * @param {string} text log message
 * @param {any} obj an object to dump (optional)
 */
export function raw(text: string, obj: any = null) {
  console.log(parseLog(text, '', obj));
}

/**
 * logs a message with "info" level
 * @param {string} text log message
 * @param {any} obj an object to dump (optional)
 */
export function info(text: string, obj: any = null) {
  console.info(parseLog(text, '[INFO] ', obj));
}

/**
 * logs an error message
 * @param {string} text message
 * @param {any} e exception object
 */
export function error(text: string, e: any = null) {
  console.error(parseLog(text, '[ERROR] ', e));
}

/**
 * logs a message with "warn" level
 * @param {string} text log message
 * @param {any} obj an object to dump (optional)
 */
export function warn(text: string, obj: any = null) {
  console.warn(parseLog(text, '[WARN] ', obj));
}

/**
 * logs a message with "debug" level
 * @param {string} text log message
 * @param {any} obj an object to dump (optional)
 */
export function debug(text: string, obj: any = null) {
  console.debug(parseLog(text, '[DEBUG] ', obj));
}
