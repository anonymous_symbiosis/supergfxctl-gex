// @ts-ignore: new import-scheme (GS >=45)
import GObject from 'gi://GObject';
// @ts-ignore: new import-scheme (GS >=45)
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';
import {
  QuickMenuToggle,
  SystemIndicator,
  // @ts-ignore: new import-scheme (GS >=45)
} from 'resource:///org/gnome/shell/ui/quickSettings.js';
// @ts-ignore: new import-scheme (GS >=45)
import { Extension } from 'resource:///org/gnome/shell/extensions/extension.js';

import * as Log from './log';
import { DBus } from './dbus';
import { LabelHelper } from '../helpers/label_helper';
import { LabelType } from '../types/label_type';

/**
 * indicator icon
 */
export const GpuModeIndicator = GObject.registerClass(
  class GpuModeIndicator extends SystemIndicator {
    constructor() {
      super();
    }
  }
);

/**
 * menu toggle (QuickToggle)
 */
export const GpuModeToggle = GObject.registerClass(
  class GpuModeToggle extends QuickMenuToggle {
    private _instance?: Extension;
    private _title = 'Graphics';
    private _dbus?: DBus;
    private _labels?: LabelHelper;
    private _gpuModeIndicator: any = null;
    private _gpuModeSection!: any;
    private _gpuModeItems = new Map();

    // implicit read inside DBus->File(.., path)
    _exPath!: string;

    constructor(gpuModeIndicator: any, instance: Extension) {
      super();
      this._instance = instance;
      this._exPath = this._instance.path ?? '';

      // set indicator (for DI)
      this._gpuModeIndicator = gpuModeIndicator;

      // initialize the indicator
      this._gpuModeIndicator._indicator =
        this._gpuModeIndicator._addIndicator();

      // populate menu
      this._gpuModeSection = new PopupMenu.PopupMenuSection();
      // @ts-ignore: DI
      this.menu.addMenuItem(this._gpuModeSection);

      // initialize DBus
      this._dbus = new DBus(this); // this also spawns the quick-toggle title and labels

      // using then and catch as we can't make this constructor async
      this._dbus
        .initialize()
        .then(() => {
          this._gpuModeIndicator._indicator.visible = true;
        })
        .catch((e: any) => {
          Log.error('Panel: Initilization: error!', e);
        })
        .finally(() => {
          this._labels = this._dbus?.labelHelper;
          this.refresh();
        });
    }

    /**
     * reloads the UI if needed (labels, icons, toggles, etc.)
     */
    public refresh(): void {
      // avoid race-condition on signal change during init
      if (this._dbus === undefined || this._labels === undefined) {
        this._instance.disable(); // shuts down the extension
        return;
      }

      this._gpuModeSection.removeAll();
      this._gpuModeItems.clear();

      const lastState = this._dbus.lastState;

      if (this._dbus.connected && this._dbus.supported.length > 0) {
        for (const key in this._dbus.supported) {
          if (Object.prototype.hasOwnProperty.call(this._dbus.supported, key)) {
            const element = this._dbus.supported[key];
            const item = new PopupMenu.PopupImageMenuItem(
              this._labels.get(LabelType.GfxMenu, element),
              this._labels.getGpuIcon(LabelType.Gfx, element)
            );
            // bind click event on mode (switch)
            item.connect('activate', () => {
              if (this._dbus !== undefined) this._dbus.gfxMode(element);
            });
            this._gpuModeItems.set(element, item);
            this._gpuModeSection.addMenuItem(item);
          }
        }
      }

      this._gpuModeSection.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

      for (const [profile, item] of this._gpuModeItems) {
        item.setOrnament(
          profile === lastState
            ? PopupMenu.Ornament.DOT
            : PopupMenu.Ornament.NONE
        );
      }

      const powerItem = new PopupMenu.PopupImageMenuItem(
        `dedicated GPU is ${this._labels.get(
          LabelType.Power,
          this._dbus.lastPowerState
        )}`,
        this._labels.getDGpuIcon(
          LabelType.PowerFileName,
          this._dbus.lastPowerState
        )
      );
      powerItem.sensitive = false;
      powerItem.active = false;

      this._gpuModeSection.addMenuItem(powerItem);

      const title = `${this._labels.get(
        LabelType.GfxMenu,
        this._dbus.lastState
      )} (${this._labels.get(LabelType.Power, this._dbus.lastPowerState)})`;

      if (this._labels.gsVersion >= 44) {
        // @ts-ignore
        this.title = this._title;
        // @ts-ignore
        this.subtitle = title;
      } // @ts-ignore
      else this.label = title;

      // @ts-ignore
      this.gicon = this._labels.getGpuIcon(
        LabelType.Gfx,
        this._dbus.lastState,
        LabelType.PowerFileName,
        this._dbus.lastPowerState
      );

      this.addIndicator();

      if (this._labels.gsVersion >= 44) {
        // @ts-ignore: with subtitle
        this.menu.setHeader(
          // @ts-ignore
          this.gicon,
          this._title + ' (SuperGFX)',
          `Current status: ${this._labels.get(
            LabelType.GfxMenu,
            this._dbus.lastState
          )} (dGPU: ${this._labels.get(
            LabelType.Power,
            this._dbus.lastPowerState
          )})`
        );
      } else {
        // @ts-ignore: no subtitle
        this.menu.setHeader(
          // @ts-ignore
          this.gicon,
          `Current status: ${this._labels.get(
            LabelType.GfxMenu,
            this._dbus.lastState
          )} (dGPU: ${this._labels.get(
            LabelType.Power,
            this._dbus.lastPowerState
          )})`
        );
      }
    }

    /**
     * adds the indicator icon
     */
    public addIndicator(): void {
      if (this._dbus === undefined || this._labels === undefined) return;

      this._gpuModeIndicator.remove_actor(this._gpuModeIndicator._indicator);
      this._gpuModeIndicator._indicator.gicon = this._labels.getGpuIcon(
        LabelType.Gfx,
        this._dbus.lastState,
        LabelType.PowerFileName,
        this._dbus.lastPowerState
      );

      this._gpuModeIndicator._indicator.style_class =
        'supergfxctl-gex panel-icon';

      this._gpuModeIndicator.add_actor(this._gpuModeIndicator._indicator);
    }

    /**
     * remove from panel
     */
    public disable(): void {
      // @ts-ignore
      this.destroy();

      this._dbus = undefined;
      this._labels = undefined;
      this._gpuModeIndicator = null;
    }
  }
);
