export const enum ActionType {
  Logout,
  Reboot,
  Integrated,
  AsusMuxDisable,
  None,
}
