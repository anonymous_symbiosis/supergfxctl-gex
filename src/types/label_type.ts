export const enum LabelType {
  Gfx,
  GfxMenu,
  Power,
  PowerFileName,
  UserAction,
}
