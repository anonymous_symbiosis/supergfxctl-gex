export const enum UrgencyLevel {
  Low,
  Normal,
  Critical,
}
